package main

import (
	"fmt"
)

func main() {

	sliceNum := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}

	for i := 1; i <= 12; i++ {

		for _, y := range sliceNum {

			fmt.Printf("%3v ", (i * y))
		}
		fmt.Printf("\n")
	}

}
