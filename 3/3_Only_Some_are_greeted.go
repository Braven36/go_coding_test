package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter Name: ")

	text, _ := reader.ReadString('\n')
	trimtext := strings.TrimRight(text, "\r\n")

	if strings.EqualFold(trimtext, "alice") || strings.EqualFold(trimtext, "bob") {

		fmt.Printf("Good Morning %v\n", text)

	} else {

		fmt.Printf("Access is Denied %v\n", text)
	}

}
