package main

import "fmt"

func main() {

	c := make(chan int)
	// Buffer Channel

	// One way to pass value into a channel then back out.
	go func() {

		c <- 42

	}()
	fmt.Println(<-c)

	// Buffer Channel
	b := make(chan int, 1)
	b <- 43

	fmt.Println(<-b)

}
