package main

import (
	"flag"
	"fmt"
)

func makeRange(max int) []int {

	a := make([]int, max)
	for i := range a {

		a[i] = i + 1

	}
	return a
}

func sumOfslice(sliceNum []int) int {
	var answer int
	for _, num := range sliceNum {
		answer = answer + num

	}
	return answer

}

func productOfslice(sliceNum []int) int {

	var answer int
	answer = 1
	for _, num := range sliceNum {

		answer = answer * num

	}
	return answer
}

func main() {

	var number = flag.Int("number", 0, "Please Provide a Number")
	var sum = flag.Bool("sum", false, "This Add the Number Range")
	var product = flag.Bool("product", false, "This Add the Number Range")
	flag.Parse()
	sliceNum := makeRange(*number)
	if *sum {

		sum := sumOfslice(sliceNum)
		fmt.Println("Your sum is: ", sum)
	}
	if *product {

		product := productOfslice(sliceNum)
		fmt.Println("Your product is: ", product)
	}

}
