package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func makeRange(max int) []int {
	var s []int
	a := make([]int, max+1)
	for i := range a {
		if i%3 == 0 || i%5 == 0 {
			s = append(s, i)

		}

	}
	return s
}

func sumOfslice(sliceNum []int) int {
	var sum int
	for i := range sliceNum {
		sum = sum + i

	}
	return sum

}

func main() {

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Please Enter a Number: ")
	scanner.Scan()
	num, err := strconv.Atoi(scanner.Text())

	if err != nil {
		log.Fatal(err)

	}

	sliceNum := makeRange(num)
	sum := sumOfslice(sliceNum)

	fmt.Println("Your sum is: ", sum)

}
